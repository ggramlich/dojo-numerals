/**
 * Created by Kazmi on 14.03.2017.
 */
public class Numerals {

    public String recursive(int i, String intermediateResult) {

        if (i >= 1000) {
            intermediateResult += "M";
            return recursive(i - 1000, intermediateResult);
        }

        if (i >= 500) {
            intermediateResult += "D";
            return recursive(i - 500, intermediateResult);
        }

        if (i >= 100) {
            intermediateResult += "C";
            return recursive(i - 100, intermediateResult);
        }

        if (i >= 50) {
            intermediateResult += "L";
            return recursive(i - 50, intermediateResult);
        }

        if (i >= 10) {
            intermediateResult += "X";
            return recursive(i - 10, intermediateResult);
        }

        if (i >= 5) {
            intermediateResult += "V";
            return recursive(i - 5, intermediateResult);
        }

        if (i >= 1) {
            intermediateResult += "I";
            return recursive(i - 1, intermediateResult);
        }

        return intermediateResult;
    }

    public String convert(int i) {
        String result = "";

        result = recursive(i, result);
//
//        while (i > 50) {
//            result += "L";
//            i = i - 50;
//        }
//
//        while (i > 10) {
//            result += "X";
//            i = i - 10;
//        }
//
//        if (i >= 5 && i < 9) {
//            result += "V";
//            i = i - 5;
//        }
//        while (i >= 1 && i < 4) {
//            result += "I";
//            i--;
//        }
//
//        if (i == 4) {
//            result = "IV";
//        }
//
//        if (i == 9) {
//            result = "IX";
//        }

        return result;
    }
}
