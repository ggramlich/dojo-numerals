import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Kazmi on 14.03.2017.
 */
public class NumeralsTest {

    Numerals testee = new Numerals();

    @Test
    public void convertOneToRoman() {
        assertEquals("I", testee.convert(1));
    }

    @Test
    public void additionOfBasicNumbers() {
        assertEquals("III", testee.convert(3));
    }

    @Test
    public void additionOfBasicNumbers5() {
        assertEquals("V", testee.convert(5));
    }

    @Test
    public void additionOfBasicNumbers6() {
        assertEquals("VI", testee.convert(6));
    }

    @Test
    public void additionOfBasicNumbers8() {
        assertEquals("VIII", testee.convert(8));
    }

    @Test
    public void subtractionOfBasicNumbers4() {
        assertEquals("IV", testee.convert(4));
    }

    @Test
    public void subtractionOfBasicNumbers9() {
        assertEquals("IX", testee.convert(9));
    }

    @Test
    public void additionOfBasicNumbers1066() {
        assertEquals("MLXVI", testee.convert(1066));
    }
}
